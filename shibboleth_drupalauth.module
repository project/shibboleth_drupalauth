<?php
/**
 * @file
 * Allows Drupal to be an authentication source for Shibboleth
 */

/**
 * Implements hook_permission().
 */
function shibboleth_drupalauth_permission() {
  return array (
    'validate_user_session' => array (
      'title' => t('Validate auth tokens'),
    ),
    'administer shibboleth drupalauth' => array (
      'title' => t('Administer Shibboleth DrupalAuth'),
    ),
  );
}


/**
 * Implements hook_menu().
 */
function shibboleth_drupalauth_menu() {
  $items = array();
  $items['admin/config/people/shibboleth_drupalauth'] = array(
    'title' => 'Shibboleth DrupalAuth Settings',
    'description' => 'Configure various shibboleth drupalauth settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('shibboleth_drupalauth_admin_settings'),
    'access arguments' => array('administer shibboleth drupalauth'),
    'file' => 'shibboleth_drupalauth.admin.inc',
  );
  $items['admin/config/people/shibboleth_drupalauth/%'] = array(
    'title' => 'Shibboleth DrupalAuth Settings',
    'description' => 'Configure various shibboleth drupalauth server.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('shibboleth_drupalauth_admin_settings_server',4),
    'access arguments' => array('administer shibboleth drupalauth'),
    'file' => 'shibboleth_drupalauth.admin.inc',
  );
  $items['admin/config/people/shibboleth_drupalauth/%/delete'] = array(
    'title' => 'Shibboleth DrupalAuth Settings',
    'description' => 'Configure various shibboleth drupalauth server.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('shibboleth_drupalauth_admin_settings_server_delete',4),
    'access arguments' => array('administer shibboleth drupalauth'),
    'file' => 'shibboleth_drupalauth.admin.inc',
  );
  $items['shibboleth_continue'] = array(
    'title' => 'Shibboleth DrupalAuth',
    'description' => 'Continue Shibboleth Login Process',
    'page callback' => 'shibboleth_drupalauth_continue',
    'access callback' => 'shibboleth_drupalauth_continue_access',
    'type' => MENU_CALLBACK,
  );
  return $items;
}


/**
 * Implements hook_init().
 */
function shibboleth_drupalauth_init() {
  global $user;
  if ($user->uid == 0) {
    return;
  }

  $s = shibboleth_drupalauth_settings();
  $token = _shibboleth_drupalauth_gen_token($user->uid, $user->mail, $user->sid, $user->ssid);

  if (!isset($_COOKIE[$s['cookiename']]) or $_COOKIE[$s['cookiename']] != $token) {
    setcookie($s['cookiename'], $token, $s['cookieexpire'], $s['cookiepath'], $s['cookiedomain']);
  }
}


/**
 *  Returns the settings
 */
function shibboleth_drupalauth_settings() {
  $settings = variable_get('shibboelth_drupalauth_settings', array());
  return $settings;
}

function shibboleth_drupalauth_save_settings($settings) {
  drupal_set_message(t('Shibboleth DrupalAuth settings saved'));
  variable_set('shibboelth_drupalauth_settings', $settings);
}

/**
 * Page callback for when a user is forwarded from Shibboleth to Drupal for Authentication
 */
function shibboleth_drupalauth_continue() {
  global $user;
  if ($user->uid == 0) {
    drupal_goto('user', array('query' => array('destination' =>'shibboleth_continue')));
  } else {
    $s = shibboleth_drupalauth_settings();
    drupal_goto($s['idpurl']);
  }
}

/**
 * Access callback for the page above.
 * This should always return true.
 */
function shibboleth_drupalauth_continue_access() {
  return TRUE;
}

/**
 * Implements hook_services_resources().
 */
function shibboleth_drupalauth_services_resources() {
  $definition = array(
    'user' => array(
      'actions' => array(
        'validate' => array(
          'help' => 'Validate Session',
          'callback' => 'shibboleth_drupalauth_validate',
          'access callback' => 'shibboleth_drupalauth_validate_access',
          'args' => array(
            array(
              'name' => 'token',
              'optional' => FALSE,
              'source' => array('data' => 'token'),
              'type' => 'string',
              'description' => 'The session key',
            ),
          ),
        ),
      ),
    ),
  );
  return $definition;
}


/**
 * Callback for the validate function
 */
function shibboleth_drupalauth_validate($token) {
  $settings = shibboleth_drupalauth_settings();
  $uid = (int)substr($token, 0, strpos($token, '-'));

  $results = db_select('sessions')
    ->fields('sessions')
    ->condition('uid', $uid)
    ->execute();

  $user = user_load($uid);

  if (isset($settings['servers'][ip_address()])) {
    $datatype = $settings['servers'][ip_address()]['principal'];
  } else if (isset($settings['servers']['any'])) {
    $datatype = $settings['servers']['any']['principal'];
  } else {
    return FALSE;
  }

  while ($result = $results->fetchAssoc()) {
    $newtoken = _shibboleth_drupalauth_gen_token($uid, $user->mail, $result['sid'], $result['ssid']);
    if ($token == $newtoken) {
      switch ($datatype) {
        case 'email':
          $userdata = $user->mail;
          break;
        case 'emailuser':
          $address = explode('@', $user->mail);
          $userdata = $address[0];
          break;
        case 'username':
        default:
          $userdata = $user->name;
          break;
      }
      return array(TRUE, $userdata, $result['hostname']);
    }
  }
  return FALSE;
}


/**
 * Access callback for service call
 */
function shibboleth_drupalauth_validate_access() {
  return TRUE;
}

/**
 * Generates the authentication token
 */
function _shibboleth_drupalauth_gen_token($uid, $mail, $sid = '', $ssid = '') {
  return $uid . '-' . preg_replace(
    '/[^a-zA-Z0-9]/',
    '',
    base64_encode(hash('sha256', $mail . $sid . $ssid, TRUE))
  );
}

/**
 * Data options available
 */
function _shibboleth_drupalauth_data_options() {
  return array(
    'username' => t('Login Username'),
    'email'    => t('Email Address'),
    'emailuser' => t('The username portion of the email address'),
  );
}
